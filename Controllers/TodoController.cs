﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace TodoApiD.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TodoController : ControllerBase
    {

  
        private static List<TodoItem> tasks = new List<TodoItem> {
            new TodoItem()  { id = 1, name = "Sacar la basura", isComplete = false},
            new TodoItem()  { id = 2, name = "Sacar a pasear al perro", isComplete = false},
            new TodoItem()  { id = 3, name = "Aprender dialogflow", isComplete = false},
            new TodoItem()  { id = 4, name = "Recibir correazos de Ernesto", isComplete = false},
        };

        private static readonly bool[] States = new[]
        {
            true, false
        };


        [HttpGet]
        [Route("[action]")]
        public List<TodoItem> Get()
        {
            return tasks;
        }
        [HttpPost]
        [Route("[action]")]
        public List<TodoItem> Create(TodoItem item)
        {
            
            TodoItem task = new TodoItem()
            {
                id = tasks.Count + 1,
                name = item.name,
                isComplete = false
            };
            tasks.Add(task);
            return tasks;
        }
        [HttpPost]
        [Route("[action]")]
        public List<TodoItem> Delete(TodoItem index)
        {
            tasks.RemoveAll(el => el.id == index.id);
            return tasks;
        }
    }
}
